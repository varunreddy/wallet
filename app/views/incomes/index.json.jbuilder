json.array!(@incomes) do |income|
  json.extract! income, :id, :category_id, :type, :string, :amount, :account
  json.url income_url(income, format: :json)
end
