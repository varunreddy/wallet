json.array!(@costs) do |cost|
  json.extract! cost, :id, :category_id, :type, :string, :amount, :account
  json.url cost_url(cost, format: :json)
end
