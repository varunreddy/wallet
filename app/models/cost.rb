class Cost < ActiveRecord::Base
  belongs_to :category
  validates :category_id, :presence => true
  validates :name, :presence => true
  validates :amount, :presence => true
  validates :account, :presence => true
  end
