class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.references :category, index: true, foreign_key: true
      t.string :name
      t.string :string
      t.integer :amount
      t.string :account

      t.timestamps null: false
    end
  end
end
